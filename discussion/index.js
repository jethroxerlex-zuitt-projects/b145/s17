console.log("Hello from JS")

//[SECTION] CONTROL STRUCTURES

//[SUB SECTION] IF-ELSE STATEMENT

let numA = -1; // we will try to assess the value.

// IF STATEMENT (Branch) - the task of the "IF" statement is to execute a procedure or action if the specified condition is "True".
if (numA <= 0) {
    //truthy branch
    //this block of code will run if the condition is MET.
    console.log('The condition was MET');
}

let name = 'Lorenzo'

//create a control structure that will allow the user to proceed if the value matches or passes the condition.
if (name === 'Lorenzo') {
    //if it passes the condition the "truthy" branch will run.
    console.log('User can proceed!')

}
let isLegal = true;
//! - NOT //false
if (!isLegal) {
    console.log('User can proceed!')
}

//[SUB SECTION] ELSE statement
//this executes a statement if ALL of the other conditions are "FALSE" and/or has failed.

//Create a control structure that will allow us to simulate a user login.


//prompt obx -> prompt(): this will allow us to display a prompt dialog box which we can ask the user for an input.
//syntax: prompt(text/message[REQUIRED],placeholder/default text[OPTIONAL]);
// prompt("Please enter your first name:", "Jethro");

//Create a control structure that will allow us to check if the user is old enough to drink.

//alert() - display a message box to the user which would require their attention
// let age = 21;
//  if (age >= 18) {
//  	//truthy branch
//  	alert("You're old enough to drink.")
//  }
//  else {
//  	//falsy branch
//  	 alert("You're not old enough to drink.") 
//  }


//Create a control structure that will ask for the number of drinks that you will order
//ask the user how many drinks he wants
// let order = prompt('How many orders of drinks do you like?');

//convert a string data type into a number data type.
//parseInt() - this will allow us to convert strings into integers.
// order = parseInt(order);
let drink = "🍸"
// let multidrink =prompt("You ordered" + drink.repeat(order));

//create a logic that will make sure that the user's input is greater than 0
// type coercion = conversion data type was converted to another data type.

//1. if one of the operands is an object, it will be converted into a primitive data type.

//in JS there are 3 ways to multiply a string.
//1. repeat() method ==> this will allow us to return a new string value that contains the number of copies of the string.
//syntax: string.repeat(value/number)
//2. loops ==> for loop
//3. loops ==> while loop method.

// if (order > 0) {
// console.log(typeof order);
// // alert( drink * order); 
// let multidrink =alert("You ordered"+ (order)+ "drinks" + drink.repeat(order));     //multiplication

// }
// else {
// 	alert('The number should be above 0');
// }
//You want to create other predetermined conditions you can create a nested(multiple) if-else statement.

//15 mins to formulate a logic that will allow us to multiply a string to a desired number.


//mini task: vaccine checker
function vaccineChecker() {
    //ask information from the user.
    let vax = prompt('What brand is your vaccine?')
    //we need to process the input of the user so that whatever input he will enter we can control the uniformity of the character casing.
    //toLowerCase() - will allow us to convert a string into all lowercase characters. syntax: string.toLowerCase()
    vax = vax.toLowerCase();
    //Create a logic that will allow us to check the values inserted by the user to match the given parameters.
    if (vax === 'pfizer' || vax === 'moderna' || vax === 'astrazenica' || vax === 'janssen') {
        //display the response back to the client.
        alert(vax + ' is allowed to travel');
    } else {
        alert('sorry not permitted');
    }

}

//for this mini task we want the user to be able to invoke this function with the use of a trigger.

// vaccineChecker();

//onclick - is an example of a JS Event. this "event" in JS occurs when the user clicks on an element/component to trigger a certain procedure.

//syntax: <element/component onclick="myScript/Function"> 

// Typhoon checker
function determineTyphoonIntensity() {
    //going to need an input from the user.
    //we need a number data type so that we can properly compare the values.
    let windspeed = parseInt(prompt('Wind speed:'));
    console.log(typeof windspeed); //this is to prove that we can directly pass the value of the variable and feed to the parseInt method.
    if (windspeed < 29) {
        alert('Not a Typhoon Yet!');
    } else if (windspeed <= 61) {
        //this will run if the 2nd statement was met
        alert('Tropical Depression Detected');
    } else if (windspeed <= 88) {
        alert('Tropical Storm Detected');
    } else if (windspeed <= 117) {
        alert('Severe Tropical Storm');
    } else {
        alert('Typhoon Detected!');
    }
}

// determineTyphoonIntensity();

//conditional ternery
//it still follows the same syntax with an if-else
//(truthy,falsy)
//this is the only JS operator that takes 3 operands
// symbols that seperates are structure
// ? question mark - this would describe a condition that if resulted to "true" will run "truthy". 
// : colon
function ageChecker() {
    let age = parseInt(prompt('How old are you?'));
    //simplify the structure below with the help of ternary operator
    //syntax: condition ? "truthy" : "falsy"

    //ternary structure is a short hand version of if-else.
    return (age >= 18) ? alert('Old enough to vote') : alert("Not old enough");

    // if (age >= 18) {
    //    //truthy
    //    alert('Old enough to vote');
    // } else {
    //   //falsy
    //   alert('Not Old enough');
    // }

}
// Create a function that will determine the owner of a computer unit.

function determineComputerOwner() {
    let unit = prompt('What is the unit no. ?');

    let user;
    //the unit - represents the unit number.
    //unit === case (you should always consider the data type.)
    //the user - represents the user who owns the unit.
    switch (unit) {
        //declare multiple cases to represent each outcome that will match the value inside the expression.
        case '1':
            user = "John Travolta";
            break;
        case '2':
            user = 'Steve Jobs';
            break;
        case '3':
            user = 'Sid Meir';
            break;
        case '4':
            user = 'Onel de guzman';
            break;
        case '5':
            user = 'Christian Salvador';
            break;


        default:
            user = 'wala yan sa options na pagpipilian';
            //if all else fails or if none of the preceeding cases above meets the expression, this statement will become the fail safe/default response.
    }
    alert(user);
}
//when to use"" (double quotes) over '' (single quotes)

//"name" === 'name'

//we can use either methods to declare a string value. howeverwe can use one over the other to ESCAPE the scope of the initial symbol.

// let dialog = '"Drink your water bhie" - mimiyuh says'
// let dialog2 = "'I shall return'-McArthur"
// let ownership = 'Aling Nena\'s Tindahan' //alternative in inserting apostrophe.

//create a demo video on the 4 functions that we have created.