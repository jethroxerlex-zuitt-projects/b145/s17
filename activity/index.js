console.log('Hello from JS!')
function determinetheEquation() {
	let numA = parseInt(prompt('Provide a number:'));
	let numB = parseInt(prompt('Provide another number:'));
	if (numA + numB < 10) {
        console.log('The sum of the two numbers are: ' + (numA + numB))
      
	}else if (numA + numB < 20) {
		alert('the difference of the two numbers are: ' + (numA - numB));

	}else if (numA + numB < 30){
		alert('the product of the two numbers are: ' + (numA * numB));
	}else {
		alert('the quotient of the two numbers are: ' + (numA / numB));
	}
} 
function userid() {
	let username = prompt('What is your name?');
	let userage = prompt('How old are you?');
	if (username === null || userage === null || username === "" || userage === "") {
		alert('Are you an alien?');
	}else {
		alert('This is ' + username + ' ' + userage + ' years old');
	}
}

function legalage() {
	let legAge = prompt('Provide an age:');
	let checkage;

	switch(legAge) {
		case '18':
		    checkage = alert('You are now allowed to party.');
		    break;
		case '21':
		   checkage = alert('You are now part of the adult society.');
		   break;
		case '65':
		   checkage = alert('We thank you for your contribution to the society');
		   break;
		default:
		   checkage = alert('Are you sure you\'re not an alien?');          

	}
}

// create a function that will determine if the age is too old for preschool

function ageChecker(){
   //Use a try-catch statement

   //get the input of the user.
   //the getElementByID() will target a component within the document using its ID attribute
   //the "document" parameter describes the HTML document/file where the JS module is linked.
   //"value" -describes the value property of our elements
   let userInput = document.getElementById('age').value;
   
   //we will now target the element where we will display the output of this function.
   let message = document.getElementById('outputdisplay');

   try {
   	//make sure that the input inserted by the user is NOT equal to a blank string.
   	//throw- this statement examines the input and returns an error.
   	console.log(typeof userInput)
   	if (userInput === '') throw 'the input is empty';
   	//create a conditional statement that will check if the input is NOT a number
   	//in order to check if the value is NOT A NUMBER, we will use a isNaN()
   	if (isNaN(userInput)) throw 'the input is Not a Number';
   	if (userInput <= 0) throw 'Not a valid Input'
   	if (userInput <= 7) throw 'Good for preschool';
   	if (userInput > 7) throw 'too old for preschool';

   }catch(err) {
      //the "err" is to define the error that will be thrown by the try section. so "err" is caught by the catch statement and a custom error message will be displayed.
      //how are we going to inject a value inside th html container?
      // using innerHTML property :
      //syntax: element.innerHTML - this will allow us to get/set the HTML markup contianed within the element.
      message.innerHTML ="Age Input: " + err; 
   }finally {
   	//this statement here will be executed regardless of the result above
   	alert('This is from the finally section');
   }
}